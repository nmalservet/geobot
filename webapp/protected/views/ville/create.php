<?php
/* @var $this VilleController */
/* @var $model Ville */

$this->breadcrumbs=array(
	'Villes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Ville', 'url'=>array('index')),
	array('label'=>'Manage Ville', 'url'=>array('admin')),
);
?>

<h1>Create Ville</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>