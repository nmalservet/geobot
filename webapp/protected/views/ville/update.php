<?php
/* @var $this VilleController */
/* @var $model Ville */

$this->breadcrumbs=array(
	'Villes'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Ville', 'url'=>array('index')),
	array('label'=>'Create Ville', 'url'=>array('create')),
	array('label'=>'View Ville', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Ville', 'url'=>array('admin')),
);
?>

<h1>Update Ville <?php echo $model->ID; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>