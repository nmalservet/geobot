<?php
/* @var $this VilleController */
/* @var $data Ville */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CODEPAYS')); ?>:</b>
	<?php echo CHtml::encode($data->CODEPAYS); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CP')); ?>:</b>
	<?php echo CHtml::encode($data->CP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('VILLE')); ?>:</b>
	<?php echo CHtml::encode($data->VILLE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NOMADMIN1')); ?>:</b>
	<?php echo CHtml::encode($data->NOMADMIN1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CODEADMIN1')); ?>:</b>
	<?php echo CHtml::encode($data->CODEADMIN1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NOMADMIN2')); ?>:</b>
	<?php echo CHtml::encode($data->NOMADMIN2); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('CODEADMIN2')); ?>:</b>
	<?php echo CHtml::encode($data->CODEADMIN2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NOMADMIN3')); ?>:</b>
	<?php echo CHtml::encode($data->NOMADMIN3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CODEADMIN3')); ?>:</b>
	<?php echo CHtml::encode($data->CODEADMIN3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LATITUDE')); ?>:</b>
	<?php echo CHtml::encode($data->LATITUDE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LONGITUDE')); ?>:</b>
	<?php echo CHtml::encode($data->LONGITUDE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ACURANCY')); ?>:</b>
	<?php echo CHtml::encode($data->ACURANCY); ?>
	<br />

	*/ ?>

</div>