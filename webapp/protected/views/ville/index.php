<?php
/* @var $this VilleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Villes',
);

$this->menu=array(
	array('label'=>'Create Ville', 'url'=>array('create')),
	array('label'=>'Manage Ville', 'url'=>array('admin')),
);
?>

<h1>Villes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
