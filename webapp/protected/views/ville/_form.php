<?php
/* @var $this VilleController */
/* @var $model Ville */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cpautocomplete-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'CODEPAYS'); ?>
		<?php echo $form->textField($model,'CODEPAYS',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'CODEPAYS'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CP'); ?>
		<?php echo $form->textField($model,'CP',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'CP'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'VILLE'); ?>
		<?php echo $form->textField($model,'VILLE',array('size'=>60,'maxlength'=>180)); ?>
		<?php echo $form->error($model,'VILLE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NOMADMIN1'); ?>
		<?php echo $form->textField($model,'NOMADMIN1',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'NOMADMIN1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CODEADMIN1'); ?>
		<?php echo $form->textField($model,'CODEADMIN1',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'CODEADMIN1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NOMADMIN2'); ?>
		<?php echo $form->textField($model,'NOMADMIN2',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'NOMADMIN2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CODEADMIN2'); ?>
		<?php echo $form->textField($model,'CODEADMIN2',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'CODEADMIN2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NOMADMIN3'); ?>
		<?php echo $form->textField($model,'NOMADMIN3',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'NOMADMIN3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CODEADMIN3'); ?>
		<?php echo $form->textField($model,'CODEADMIN3',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'CODEADMIN3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'LATITUDE'); ?>
		<?php echo $form->textField($model,'LATITUDE'); ?>
		<?php echo $form->error($model,'LATITUDE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'LONGITUDE'); ?>
		<?php echo $form->textField($model,'LONGITUDE'); ?>
		<?php echo $form->error($model,'LONGITUDE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ACURANCY'); ?>
		<?php echo $form->textField($model,'ACURANCY'); ?>
		<?php echo $form->error($model,'ACURANCY'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->