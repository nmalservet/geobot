<?php
/* @var $this VilleController */
/* @var $model Ville */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CODEPAYS'); ?>
		<?php echo $form->textField($model,'CODEPAYS',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CP'); ?>
		<?php echo $form->textField($model,'CP',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'VILLE'); ?>
		<?php echo $form->textField($model,'VILLE',array('size'=>60,'maxlength'=>180)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NOMADMIN1'); ?>
		<?php echo $form->textField($model,'NOMADMIN1',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CODEADMIN1'); ?>
		<?php echo $form->textField($model,'CODEADMIN1',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NOMADMIN2'); ?>
		<?php echo $form->textField($model,'NOMADMIN2',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CODEADMIN2'); ?>
		<?php echo $form->textField($model,'CODEADMIN2',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NOMADMIN3'); ?>
		<?php echo $form->textField($model,'NOMADMIN3',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CODEADMIN3'); ?>
		<?php echo $form->textField($model,'CODEADMIN3',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'LATITUDE'); ?>
		<?php echo $form->textField($model,'LATITUDE'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'LONGITUDE'); ?>
		<?php echo $form->textField($model,'LONGITUDE'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ACURANCY'); ?>
		<?php echo $form->textField($model,'ACURANCY'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->