<?php
/* @var $this VilleController */
/* @var $model Ville */

$this->breadcrumbs=array(
	'Villes'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Ville', 'url'=>array('index')),
	array('label'=>'Create Ville', 'url'=>array('create')),
	array('label'=>'Update Ville', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Ville', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ville', 'url'=>array('admin')),
);
?>

<h1>View Ville #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'CODEPAYS',
		'CP',
		'VILLE',
		'NOMADMIN1',
		'CODEADMIN1',
		'NOMADMIN2',
		'CODEADMIN2',
		'NOMADMIN3',
		'CODEADMIN3',
		'LATITUDE',
		'LONGITUDE',
		'ACURANCY',
	),
)); ?>
