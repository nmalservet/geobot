<?php

/**
 * This is the model class for table "cp_autocomplete".
 *
 * The followings are the available columns in table 'cp_autocomplete':
 * @property integer $ID
 * @property string $CODEPAYS
 * @property string $CP
 * @property string $VILLE
 * @property string $NOMADMIN1
 * @property string $CODEADMIN1
 * @property string $NOMADMIN2
 * @property string $CODEADMIN2
 * @property string $NOMADMIN3
 * @property string $CODEADMIN3
 * @property double $LATITUDE
 * @property double $LONGITUDE
 * @property integer $ACURANCY
 */
class Ville extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ville';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('CODEPAYS, CP, VILLE, NOMADMIN1, CODEADMIN1, NOMADMIN2, CODEADMIN2, NOMADMIN3, CODEADMIN3, LATITUDE, LONGITUDE, ACURANCY', 'required'),
			array('ACURANCY', 'numerical', 'integerOnly'=>true),
			array('LATITUDE, LONGITUDE', 'numerical'),
			array('CODEPAYS', 'length', 'max'=>2),
			array('CP', 'length', 'max'=>10),
			array('VILLE', 'length', 'max'=>180),
			array('NOMADMIN1, NOMADMIN2, NOMADMIN3', 'length', 'max'=>100),
			array('CODEADMIN1, CODEADMIN2, CODEADMIN3', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, CODEPAYS, CP, VILLE, NOMADMIN1, CODEADMIN1, NOMADMIN2, CODEADMIN2, NOMADMIN3, CODEADMIN3, LATITUDE, LONGITUDE, ACURANCY', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'CODEPAYS' => 'Codepays',
			'CP' => 'Cp',
			'VILLE' => 'Ville',
			'NOMADMIN1' => 'Nomadmin1',
			'CODEADMIN1' => 'Codeadmin1',
			'NOMADMIN2' => 'Nomadmin2',
			'CODEADMIN2' => 'Codeadmin2',
			'NOMADMIN3' => 'Nomadmin3',
			'CODEADMIN3' => 'Codeadmin3',
			'LATITUDE' => 'Latitude',
			'LONGITUDE' => 'Longitude',
			'ACURANCY' => 'Acurancy',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('CODEPAYS',$this->CODEPAYS,true);
		$criteria->compare('CP',$this->CP,true);
		$criteria->compare('VILLE',$this->VILLE,true);
		$criteria->compare('NOMADMIN1',$this->NOMADMIN1,true);
		$criteria->compare('CODEADMIN1',$this->CODEADMIN1,true);
		$criteria->compare('NOMADMIN2',$this->NOMADMIN2,true);
		$criteria->compare('CODEADMIN2',$this->CODEADMIN2,true);
		$criteria->compare('NOMADMIN3',$this->NOMADMIN3,true);
		$criteria->compare('CODEADMIN3',$this->CODEADMIN3,true);
		$criteria->compare('LATITUDE',$this->LATITUDE);
		$criteria->compare('LONGITUDE',$this->LONGITUDE);
		$criteria->compare('ACURANCY',$this->ACURANCY);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cpautocomplete the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
